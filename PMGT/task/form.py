from django import forms
from django.forms import ModelForm

from .models import Project, task


class DateInput(forms.DateInput):
    input_type = 'date'


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ['title','date_posted', 'description']
        widgets = {
            'date_posted': DateInput(),
        }

class TaskUpdateForm(ModelForm):
    class Meta:
        model = task
        fields = ['task_name', 'plan_start_date','plan_end_date', 'task_price', 'task_prog', 'actual_start_date','actual_end_date']
        widgets = {
            'plan_start_date': DateInput(),
            'plan_end_date': DateInput(),
            'actual_start_date': DateInput(),
            'actual_end_date': DateInput(),
        }

class TaskCreateForm(ModelForm):
    class Meta:
        model = task
        fields = ['project',  'task_name', 'plan_start_date','plan_end_date', 'task_price']
        widgets = {
            'plan_start_date': DateInput(),
            'plan_end_date': DateInput(),
        }