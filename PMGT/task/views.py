from _ast import Add
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Count
from django.db.models import Sum
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from cost.models import actual_cost
from task.form import ProjectForm, TaskUpdateForm, TaskCreateForm
from task.models import Project, task
from task.serializer import taskSerializer


class projectListView(ListView):
    model = Project
    template_name = 'task/home.html'
    base_template_name = 'task/base.html'
    context_object_name = 'projects'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project_elements = Project.objects.all()
        context['all_projects'] = Project.objects.all().count() # Count the Number of Projects Running

        finished_projects = []
        inprocess_projects = []
        project_with_no_task = []
        for x in project_elements:
            task_count = task.objects.filter(project_id=x.id).count()*100
            project_inprogress = task.objects.filter(project_id=x.id).aggregate(Sum('task_prog')).get('task_prog__sum')
            if task_count != 0:
                project_inprogress = project_inprogress/task_count
                if project_inprogress < 1:
                    inprocess_projects.append(1)
                else:
                    finished_projects.append(1)
            else:
                project_with_no_task.append(1)
        inprogress = len(inprocess_projects)
        finished = len(finished_projects)
        new_project = len(project_with_no_task)
        context['Project_under_progress']=inprogress
        context['project_finished']=finished
        context['project_no_task']=new_project
        print(context['project_no_task'])
        return context


class projectDetailView(DetailView):
    model = Project
    template_name = 'task/project_detail.html'
    other_template_name = 'task/base.html'
    home_template_name = 'task/home.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # task_data = task.objects.filter(project=self.kwargs['pk'])
        project_cost = task.objects.filter(project_id=self.kwargs['pk'])
        project_cost_serializer = taskSerializer(project_cost, many=True)
        context['tasks'] = project_cost_serializer.data

        context['count'] = task.objects.filter(project=self.kwargs['pk']).annotate(
            num_comments=Count('task_name')
        )
        context['under_process'] = context['count'].filter(task_prog__lt=100)
        context['complete'] = context['count'].filter(task_prog=100)

        earn_budget = 0
        proj_elements = Project.objects.get(id=self.kwargs['pk'])
        for index, x in enumerate(context['tasks']):
            if index < proj_elements.current_week:
                taskprice = x['task_price']
                taskpercent = x['task_prog']
                earn_budgets = (taskprice*taskpercent)/100
                earn_budget = earn_budget + earn_budgets
        context['earn_budget'] = earn_budget

        context['total'] = task.objects.filter(project=self.kwargs['pk']).aggregate(Sum('task_price')).get('task_price__sum')
        context['total_prog'] = task.objects.filter(project=self.kwargs['pk']).aggregate(Sum('task_prog')).get('task_prog__sum')
        return context



class projectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    # fields = ['title','date_posted', 'description']
    form_class = ProjectForm
    template_name = 'task/project_creat.html'
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class projectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    # fields = ['title','date_posted', 'description']
    template_name = 'task/project_update.html'



class projectDeleteView(LoginRequiredMixin,  DeleteView):
    model = Project
    template_name = 'task/project_delete.html'
    success_url = '/'

class projectweek_UpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    fields = ['current_week']
    template_name = 'task/week_update.html'




# Create your Task views here.


class taskCreateView(LoginRequiredMixin,CreateView):
    model = task
    form_class = TaskCreateForm
    # fields = ['project',  'task_name', 'plan_start_date','plan_end_date', 'task_price']
    template_name = 'task/task_create.html'


class taskUpdateView(LoginRequiredMixin,UpdateView):
    model = task
    form_class = TaskUpdateForm
    # fields = ['task_name', 'plan_start_date','plan_end_date', 'task_price', 'task_prog', 'actual_start_date','actual_end_date']
    template_name = 'task/task_update.html'



class taskDeleteView(LoginRequiredMixin,DeleteView):
    model = task
    template_name = 'task/task_delete.html'
    success_url = '/'