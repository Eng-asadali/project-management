from django.db.models import Sum
from rest_framework.fields import SerializerMethodField, CharField, IntegerField, BooleanField
from rest_framework.serializers import ModelSerializer, ImageField, Serializer
from rest_framework import serializers

from cost.models import actual_cost
from task.models import task, Project


class taskSerializer(ModelSerializer):
    actual_cost_calculated = SerializerMethodField('get_data', required=False)

    def get_data(self, obj):
        try:
            data = actual_cost.objects.filter(task_cost_id=obj.id)
            total_cost_actual = 0
            for x in data:
                cost = x.cost_amount
                total_cost_actual = total_cost_actual + cost
            return str(total_cost_actual)
        except Exception as e:
            print(e)
            return 0

    class Meta:
        model = task
        fields = '__all__'



