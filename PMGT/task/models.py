from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


# Create your models here.
class Project(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    date_posted = models.DateField(max_length=10, null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)
    duration_in_weeks = models.IntegerField(default=9)
    current_week = models.IntegerField(default=1)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('project-detail', kwargs= {'pk': self.pk})



class task(models.Model):
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    task_name = models.CharField (max_length=200, null=True, blank=True)
    plan_start_date = models.DateField (max_length=20, null=True, blank=True)
    plan_end_date = models.DateField (max_length=20, null=True, blank=True)
    task_price = models.FloatField (max_length=8, null=True, blank=True)
    task_prog = models.IntegerField(null=True, blank=True, default=0)
    actual_start_date = models.DateField(max_length=20, null=True, blank=True)
    actual_end_date = models.DateField(max_length=20, null=True, blank=True)
    actual_cost_data = models.FloatField(max_length=20,null=True, blank=True)

    def __str__(self):
        return self.task_name

    def get_absolute_url(self):
        return reverse('project-detail', kwargs={'pk':self.project.id})

