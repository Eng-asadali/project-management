# Generated by Django 3.2.2 on 2021-05-15 04:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0009_auto_20210515_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='task_end_date',
            field=models.DateField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='task_start_date',
            field=models.DateField(blank=True, max_length=20, null=True),
        ),
    ]
