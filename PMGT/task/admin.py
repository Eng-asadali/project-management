from django.contrib import admin

# Register your models here.
from task.models import Project, task

admin.site.register(Project)
admin.site.register(task)
