from django.db import models
from django.urls import reverse
from task.models import task, Project



class cost (models.Model):
    CURRENT_STATUS_CHOICES = (
        ("Proposed", "Proposed"),
        ("Approved", "Approved"),
        ("Rejected", "Rejected"),
    )

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task_Variance = models.ForeignKey(task, on_delete=models.CASCADE)
    topic = models.CharField(max_length=200, null=True, blank=True)
    cost = models.FloatField (max_length=20, null=True, blank=True)
    reason_for_Change= models.CharField(max_length=30, null=True, blank=True)
    cause_by_party = models.CharField(max_length=25, null=True, blank=True, default='-')
    purposed_start_date = models.DateField(max_length=3, null=True, blank=True)
    purposed_finish_date = models.DateField(max_length=20, null=True, blank=True)
    detail_description= models.TextField(max_length=500, null=True, blank=True)
    current_status= models.CharField(max_length=50, choices=CURRENT_STATUS_CHOICES, default='Proposed')

    def __str__(self):
        return self.topic

    def get_absolute_url(self):
        return reverse('variance-cost-detail', kwargs={'pk': self.pk})



class actual_cost (models.Model):
    task_cost = models.ForeignKey(task, on_delete=models.CASCADE)
    project_cost = models.ForeignKey(Project, on_delete=models.CASCADE)
    cost_account = models.CharField(max_length=8, null=True, blank=True)
    cost_amount = models.FloatField (max_length=20, null=True, blank=True)
    actual_cost_date= models.DateField(max_length=8, null=True, blank=True)
    invoice_date = models.DateField(max_length=8, null=True, blank=True)
    cost_name = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.cost_name

    def get_absolute_url(self):
        return reverse('actual-cost-detail', kwargs={'pk':self.project_cost.id})
