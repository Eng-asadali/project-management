
from django.urls import path
from cost.views import cost_ListView, cost_DetailView, actual_cost_DetailView, actual_cost_ListView, \
    actual_cost_CreateView, actual_cost_UpdateView, actual_cost_DeleteView, variance_cost_CreateView, \
    varianc_cost_UpdateView, variance_cost_DeleteView, variance_cost_DetailView, tg_varianc_cost_UpdateView
from cost import views as cost_views

urlpatterns = [
    path('variance',cost_ListView.as_view(), name = 'cost-home' ),

    path('variance/<int:pk>/', cost_DetailView.as_view(), name='cost-detail'),
    path('variance/new/',variance_cost_CreateView.as_view(), name = 'variance-cost-create' ),
    path('cost_variance/<int:pk>/',variance_cost_DetailView.as_view(), name = 'variance-cost-detail' ),
    path('variance/<int:pk>/update',varianc_cost_UpdateView.as_view(), name = 'variance-cost-update' ),
    path('tg_variance/<int:pk>/update',tg_varianc_cost_UpdateView.as_view(), name = 'tg_variance-cost-update' ),
    path('variance/<int:pk>/delete', variance_cost_DeleteView.as_view(), name='variance-cost-delete'),
    path('var/new/', cost_views.var_cost_create, name='variance-create'),

    path('actual',actual_cost_ListView.as_view(), name = 'actual-cost-home' ),
    path('actual/<int:pk>/', actual_cost_DetailView.as_view(), name='actual-cost-detail'),

    path('actual/new/',actual_cost_CreateView.as_view(), name = 'actual-cost-create' ),
    path('actual/<int:pk>/update',actual_cost_UpdateView.as_view(), name = 'actual-cost-update' ),
    path('actual/<int:pk>/delete',actual_cost_DeleteView.as_view(), name = 'actual-cost-delete' ),

    ]