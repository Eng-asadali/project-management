from django.contrib import admin

from.models import cost, actual_cost

admin.site.register(cost)
admin.site.register(actual_cost)
