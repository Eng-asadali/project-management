# Generated by Django 3.2.3 on 2021-05-23 14:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0029_merge_20210523_0607'),
        ('cost', '0004_alter_cost_project'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cost',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='task.project'),
        ),
    ]
