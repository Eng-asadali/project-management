# Generated by Django 3.2.3 on 2021-05-24 17:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cost', '0007_actual_cost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actual_cost',
            name='actual_cost_date',
            field=models.DateField(blank=True, max_length=8, null=True),
        ),
        migrations.AlterField(
            model_name='actual_cost',
            name='cost_account',
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name='actual_cost',
            name='cost_name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='actual_cost',
            name='invoice_date',
            field=models.DateField(blank=True, max_length=8, null=True),
        ),
    ]
