from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Count
from django.db.models import Sum
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from task.models import Project, task
from django.shortcuts import render

from .form import VarianceCostCreateForm, VarianceCostUpdateForm, ActualCostCreateForm
from .models import cost, actual_cost


class cost_ListView(ListView):
    model = Project
    template_name = 'cost/cost_home.html'
    context_object_name = 'projects'

class variance_cost_CreateView(LoginRequiredMixin, CreateView):
    model = cost
    form_class = VarianceCostCreateForm
    # fields = '__all__'
    template_name = 'cost/variance_cost_creat.html'
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class variance_cost_DetailView(DetailView):
    model = cost
    template_name = 'cost/variance_cost_detail.html'

class varianc_cost_UpdateView(LoginRequiredMixin, UpdateView):
    model = cost
    form_class = VarianceCostUpdateForm
    # fields = ['task_Variance', 'topic', 'cost', 'cause_by_party', 'purposed_start_date','purposed_finish_date', 'detail_description']
    template_name = 'cost/variance_cost_update.html'

class tg_varianc_cost_UpdateView(LoginRequiredMixin, UpdateView):
    model = cost
    fields = ['reason_for_Change', 'current_status']
    template_name = 'cost/variance_status_update.html'


class variance_cost_DeleteView(LoginRequiredMixin,  DeleteView):
    model = cost
    template_name = 'cost/variance_cost_delete.html'
    success_url = 'cost/cost_home.html'


def var_cost_create(request):
    if request.method == "POST":
        task_Variance = request.POST['task_Variance']
        project = request.POST['project']
        topic = request.POST['topic']
        cost = request.POST['cost']
        reason_for_Change = request.POST['reason_for_Change']
        cause_by_party = request.POST['cause_by_party']
        purposed_start_date = request.POST['purposed_start_date']
        purposed_finish_date = request.POST['purposed_finish_date']
        detail_description = request.POST['detail_description']
        current_status = request.POST['current_status']
        ins = cost(task_Variance=task_Variance, project=project, topic=topic, cost=cost, reason_for_Change=reason_for_Change, cause_by_party=cause_by_party,
                   purposed_start_date=purposed_start_date,purposed_finish_date=purposed_finish_date, detail_description=detail_description, current_status=current_status)
        ins.save()
    return render(request, 'cost/variance_detail.html')


class cost_DetailView(DetailView):
    model = Project
    template_name = 'cost/cost_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['xcost'] = cost.objects.filter(project_id = self.kwargs['pk'])
        context['cost_total'] = cost.objects.filter(project_id=self.kwargs['pk']).aggregate(Sum('cost')).get(
            'cost__sum')
        context ['proposed_total'] = context['xcost'].filter(current_status='Proposed').aggregate(Sum('cost')).get(
            'cost__sum')
        if context ['proposed_total']== None :
            context['proposed_total'] = 0

        context ['approved_total'] = context['xcost'].filter(current_status='Approved').aggregate(Sum('cost')).get(
            'cost__sum')
        if context ['approved_total']== None :
            context['approved_total'] = 0

        context ['rejected_total'] = context['xcost'].filter(current_status='Rejected').aggregate(Sum('cost')).get(
            'cost__sum')
        if context ['rejected_total']== None:
            context['rejected_total'] = 0




        context ['count_cost_total'] = cost.objects.filter(project_id = self.kwargs['pk']).count()
        context ['Count_proposed_total'] = context['xcost'].filter(current_status='Proposed').count()
        context ['Count_Approved_total'] = context['xcost'].filter(current_status='Approved').count()
        context ['Count_rejected_total'] = context['xcost'].filter(current_status='Rejected').count()
        return context

class actual_cost_ListView(ListView):
    model = Project
    template_name = 'cost/actual_cost_home.html'
    context_object_name = 'projects'


class actual_cost_DetailView(DetailView):
    model = Project
    template_name = 'cost/actual_cost_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['actual_cost'] = actual_cost.objects.filter(project_cost_id = self.kwargs['pk'])
        context['ddd'] = actual_cost.objects.filter(task_cost_id = self.kwargs['pk']).aggregate(Sum('cost_amount')).get(
            'cost_amount__sum')
        context['total_actual_cost'] = actual_cost.objects.filter(project_cost_id = self.kwargs['pk']).aggregate(Sum('cost_amount')).get('cost_amount__sum')
        # context['xx'] = actual_cost.objects.filter(task_cost = self.kwargs['pk']).aggregate(Sum('cost_amount')).get('cost_amount__sum')
        context['actual_cost_items_count'] = actual_cost.objects.filter(project_cost_id = self.kwargs['pk']).count()

        return context

class actual_cost_CreateView(LoginRequiredMixin, CreateView):
    model = actual_cost
    form_class = ActualCostCreateForm
    # fields = '__all__'
    template_name = 'cost/actual_cost_creat.html'
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class actual_cost_UpdateView(LoginRequiredMixin, UpdateView):
    model = actual_cost
    form_class = ActualCostCreateForm
    # fields = '__all__'
    template_name = 'cost/actual_cost_update.html'



class actual_cost_DeleteView(LoginRequiredMixin,  DeleteView):
    model = actual_cost
    template_name = 'cost/actual_cost_delete.html'
    success_url = 'cost/actual_cost_home.html'
