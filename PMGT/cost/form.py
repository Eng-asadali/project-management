from django import forms
from cost.models import cost, actual_cost
from django.forms import ModelForm

class DateInput(forms.DateInput):
    input_type = 'date'


class VarianceCostCreateForm(ModelForm):
    class Meta:
        model = cost
        fields = ['project','task_Variance', 'topic', 'cost', 'cause_by_party', 'purposed_start_date', 'purposed_finish_date', 'detail_description']
        widgets = {
            'purposed_start_date': DateInput(),
            'purposed_finish_date': DateInput(),
        }

class VarianceCostUpdateForm(ModelForm):
    class Meta:
        model = cost
        fields = ['task_Variance', 'topic', 'cost', 'cause_by_party', 'purposed_start_date', 'purposed_finish_date', 'detail_description']
        widgets = {
            'purposed_start_date': DateInput(),
            'purposed_finish_date': DateInput(),
        }

class ActualCostCreateForm(ModelForm):
    class Meta:
        model = actual_cost
        fields = ['task_cost', 'project_cost', 'cost_account', 'cost_amount', 'actual_cost_date', 'invoice_date', 'cost_name']
        widgets = {
            'actual_cost_date': DateInput(),
            'invoice_date': DateInput(),
        }



