from django.contrib.auth.mixins import LoginRequiredMixin
from task.serializer import taskSerializer

from django.db.models import Count
from task.models import Project
from cost.models import actual_cost
from task.models import task
from django.db.models import Sum
from django.views.generic import ListView, DetailView, UpdateView, CreateView

from .serializer import taskforcastSerializer


class forecast_ListView(ListView):
    model = Project
    template_name = 'forecast/forecast_home.html'
    context_object_name = 'projects'


class forecast_DetailView(DetailView):
    model = Project
    template_name = 'forecast/forecast_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['forecast'] = task.objects.filter(project_id=self.kwargs['pk'])
        context['forecast_total_budget'] = task.objects.filter(project_id=self.kwargs['pk']).aggregate(Sum('task_price')).get(
            'task_price__sum')
        context['count11'] = task.objects.filter(project=self.kwargs['pk']).annotate(
            num_comments=Count('task_name')).count()
        proj_elements = Project.objects.get(id=self.kwargs['pk'])
        select_week = context['count11']
       #>>>>>>>>>>>>> Planned Value >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
        p_value = 0
        for index, x in enumerate (context['forecast']):
            if index < proj_elements.current_week:
                taskprice = x.task_price
                p_value = p_value + taskprice
        context['forecast_total_plan'] = p_value
        # >>>>>>>>>>>>> Earned Value >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
        earn_budget = 0
        for index, x in enumerate (context['forecast']):
            if index < proj_elements.current_week:
                taskprice = x.task_price
                taskpercent = x.task_prog
                earn_budgets = (taskprice * taskpercent) / 100
                earn_budget = earn_budget + earn_budgets
        context['forecast_total_earn'] = earn_budget

        #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#

        task_data = task.objects.all()
        task_data_serializer = taskforcastSerializer(task_data, many=True)
        context['forecast_total_actual'] = task_data_serializer.data[select_week - 1]
        #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
        if context['forecast_total_plan'] == 0:
            context['SPI']=0
        else:context['SPI'] = context['forecast_total_earn']  / context['forecast_total_plan']
        if context['forecast_total_actual']['actual_cost'] == 0:
            context['CPI']=0
        else:context['CPI'] = context['forecast_total_earn'] / context['forecast_total_actual']['actual_cost']
        if context['SPI'] == 0:
            context['s1_time'] = 0
        else:context['s1_time'] = context['count11'] / context['SPI']
        context['s2_time'] = (1 + (1 - context['SPI'])) * context['count11']
        if context['CPI'] == 0:
            context['s1_cost']=0
        else:context['s1_cost'] = context['forecast_total_budget'] / context['CPI']
        if context['forecast_total_budget'] is None or 0:
            context['s2_cost'] = 0
        else:context['s2_cost'] = (1 + (1 - context['CPI'])) * context['forecast_total_budget']
        if context['forecast_total_budget'] is None or 0:
            context['cost_forecast']=0
        else:context['cost_forecast'] = context['forecast_total_budget'] + context['forecast_total_actual']['actual_cost'] - context[
            'forecast_total_earn']

        return context

class forecastweek_UpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    fields = ['current_week']
    template_name = 'forecast/week_update.html'
    success_url = '/'

