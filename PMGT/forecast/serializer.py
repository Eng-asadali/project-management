from rest_framework.fields import SerializerMethodField, CharField, IntegerField, BooleanField
from rest_framework.serializers import ModelSerializer, ImageField, Serializer
from rest_framework import serializers
from cost.models import actual_cost
from task.models import task, Project


class taskforcastSerializer(ModelSerializer):
    # budget = SerializerMethodField('get_budget', required=False)
    # planned_value = SerializerMethodField('get_planned_value', required=False)
    # earn_budget = SerializerMethodField('get_earn_budget', required=False)
    actual_cost = SerializerMethodField('get_actual_cost', required=False)


    # def get_budget(self, obj):
    #     try:
    #         total_budget = task.objects.all()
    #         budget_cal = 0
    #         for x in total_budget:
    #             budget_cal = budget_cal + x.task_price
    #         return budget_cal
    #     except Exception as e:
    #         print("Exception...", e)
    #         return 0
    #
    # def get_planned_value(self, obj):
    #     try:
    #         total_budget = task.objects.get(id = obj.id)
    #         planned_value = total_budget.task_price
    #         return planned_value
    #     except Exception as e:
    #         print("Exception...", e)
    #         return 0
    #
    # def get_earn_budget(self, obj, ):
    #
    #     try:
    #         week = obj.id
    #         earn_value = 0
    #         for x in range(week):
    #             total_budget = task.objects.get(id=x+1)
    #             total_budget = (total_budget.task_price * total_budget.task_prog)/100
    #             earn_value = earn_value + total_budget
    #         return earn_value
    #     except Exception as e:
    #         print("Exception...", e)
    #         return 0

    def get_actual_cost(self, obj):

        try:
            week = obj.id
            actual_cost_cal = 0
            for x in range(week):
                if x < 1:
                    data = actual_cost.objects.filter(task_cost_id=x+1)
                    total_cost_actual = 0
                    for x in data:
                        cost = x.cost_amount
                        total_cost_actual = total_cost_actual + cost
                    actual_cost_cal = actual_cost_cal + total_cost_actual
            return actual_cost_cal
        except Exception as e:
            print("Exception...", e)
            return 0
    class Meta:
        model = task
        fields = ['actual_cost' ]