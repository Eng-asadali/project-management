from django.urls import path

from forecast.views import forecast_ListView, forecast_DetailView, forecastweek_UpdateView

urlpatterns = [
    path('forecast',forecast_ListView.as_view(), name = 'forecast-home' ),
    path('forecast/<int:pk>/',forecast_DetailView.as_view(), name = 'forecast-detail' ),
    path('forecast/<int:pk>/update',forecastweek_UpdateView.as_view(), name = 'forecast-update' ),


    ]