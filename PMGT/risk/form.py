from django import forms
from django.forms import ModelForm
from task.models import task
from risk.models import risk


class DateInput(forms.DateInput):
    input_type = 'date'


class RiskActionUpdateForm(ModelForm):
    class Meta:
        model = risk
        fields = ['risk_action','risk_action_description','risk_action_deadline']
        widgets = {
            'risk_action_deadline': DateInput(),
        }

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#

class TaskForm(forms.ModelForm):
    class Meta:
        model = risk
        fields = ('risk_project', 'risk_key','risk_name','risk_origin','risk_likelyhood','risk_impact')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['risk_key'].queryset = task.objects.none()
