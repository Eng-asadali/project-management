from django.urls import path

from risk.views import risk_home_ListView, risk_list_DetailView, risk_DetailView, risk_CreateView, risk_UpdateView, \
    risk_DeleteView, risk_action_UpdateView

urlpatterns = [
    path('risk/',risk_home_ListView.as_view(), name = 'risk-home' ),
    path('risk/<int:pk>/',risk_list_DetailView.as_view(), name = 'risk-list-detail' ),
    path('risk_detail/<int:pk>/',risk_DetailView.as_view(), name = 'risk-detail' ),
    path('risk_detail/<int:pk>/update',risk_UpdateView.as_view(), name = 'risk-update' ),
    path('risk_action_detail/<int:pk>/update',risk_action_UpdateView.as_view(), name = 'risk-action-update' ),
    path('risk_detail/<int:pk>/delete',risk_DeleteView.as_view(), name = 'risk-delete' ),
    path('risk_detail/new/',risk_CreateView.as_view(), name = 'risk-create' ),

    ]