from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from task.models import Project, task
from .form import TaskForm, RiskActionUpdateForm
from .models import risk
from django.shortcuts import render


class risk_home_ListView(ListView):
    model = Project
    template_name = 'risk/risk_home.html'
    context_object_name = 'projects'

class risk_list_DetailView(DetailView):
    model = Project
    template_name = 'risk/risk_list_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['risks'] = risk.objects.filter(risk_project_id=self.kwargs['pk'])
        context['risk_count'] = context['risks'].count()

        likelyhood = 0
        for x in context['risks']:
            risklikely= x.risk_likelyhood
            likelyhood = likelyhood + risklikely
        if context['risk_count']== 0:
            context['average_risk_likely_hood'] = 0
        else:context['average_risk_likely_hood']=likelyhood/context['risk_count']
        riskimpact = 0
        for x in context['risks']:
            impact = x.risk_impact
            riskimpact = riskimpact + impact
        if context['risk_count'] == 0:
            context['average_risk_imapact'] = 0
        else:context['average_risk_imapact']=riskimpact/context['risk_count']
        if context['average_risk_imapact']== 0:
            context['average_risk_score'] = 0
        else:context['average_risk_score'] = context['average_risk_likely_hood']*context['average_risk_imapact']
        return context

class risk_DetailView(DetailView):
    model = risk
    template_name = 'risk/risk_detail.html'



class risk_CreateView(LoginRequiredMixin, CreateView):
    model = risk
    # form_class = TaskForm
    fields = ['risk_project', 'risk_key','risk_name','risk_origin','risk_likelyhood','risk_impact']
    template_name = 'risk/risk_create.html'

class risk_UpdateView(LoginRequiredMixin, UpdateView):
    model = risk
    # form_class = TaskForm
    fields = ['risk_project', 'risk_key','risk_name','risk_origin','risk_likelyhood','risk_impact']
    template_name = 'risk/risk_update.html'

# def load_task(request):
#     risk_project_id = request.GET.get('risk_project')
#     tasks = task.objects.filter(risk_project_id=risk_project_id).order_by('name')
#     return render(request, 'risk/risk_create.html', {'tasks': tasks})

class risk_action_UpdateView(LoginRequiredMixin, UpdateView):
    model = risk
    form_class = RiskActionUpdateForm
    # fields = ['risk_action','risk_action_description','risk_action_deadline']
    template_name = 'risk/risk_action_update.html'

class risk_DeleteView(LoginRequiredMixin, DeleteView):
    model = risk
    template_name = 'risk/risk_delete.html'
    success_url = '/risk/risk'


