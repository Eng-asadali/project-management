from django.db import models
from task.models import task, Project
from django.urls import reverse


class risk  (models.Model):
    class likelyhood (models.IntegerChoices):
        Extreme_Unlikely = 1
        Unlikely = 2
        Likely = 3
        Very_Likely= 4
        Extreme_Likely= 5

    class impact (models.IntegerChoices):
        Extreme = 1
        Extreme_low = 2
        Very_low = 3
        Medium_high = 4
        Extreme_high = 5

    RISK_ACTION_CHOICES = (
        ("Accept", "Accept"),
        ("Mitigate", "Mitigate"),
        ("Avoid", "Avoid"),
    )

    risk_key = models.ForeignKey(task, on_delete=models.CASCADE)
    risk_project = models.ForeignKey(Project, on_delete=models.CASCADE)
    risk_name = models.CharField(max_length=50)
    risk_origin= models.CharField(max_length=50, null=True, blank=True)
    risk_likelyhood = models.IntegerField(choices=likelyhood.choices)
    risk_impact = models.IntegerField(choices=impact.choices)
    risk_action  = models.CharField(max_length=50,choices=RISK_ACTION_CHOICES)
    risk_action_description  = models.CharField(max_length=100,blank=True, null=True)
    risk_action_deadline  = models.DateField(max_length=10,blank=True, null=True)


    def __str__(self):
        return self.risk_name

    def get_absolute_url(self):
        return reverse('risk-detail', kwargs={'pk': self.pk} )


